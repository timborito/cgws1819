#include "vector.h"
#include <assert.h>
#include <math.h>
#include <string>
#define EPSILON 1e-5


Vector::Vector( float x, float y, float z)
{
    this->X = x;
    this->Y = y;
    this->Z = z;
}
Vector::Vector(const Vector &v)
{
    this->X = v.X;
    this->Y = v.Y;
    this->Z = v.Z;
}

Vector::Vector()
{
    this->X = 0;
    this->Y = 0;
    this->Z = 0;
}

float Vector::dot(const Vector& v) const
{
	return ( this->X * v.X + this->Y * v.Y + this->Z * v.Z );
}

Vector Vector::cross(const Vector& v) const
{
    Vector vn;
    
    vn.X = ( this->Y * v.Z ) - (this->Z * v.Y);
    vn.Y = ( this->Z * v.X ) - (this->X * v.Z);
    vn.Z = ( this->X * v.Y ) - (this->Y * v.X);
    
    return vn;
}

bool Vector::equals(const Vector &v) const
{
    return (this->X==v.X && this->Y==v.Y && this->Z==v.Z);
}


Vector Vector::operator+(const Vector& v) const
{
    Vector vn;
    vn.X = this->X + v.X;
    vn.Y = this->Y + v.Y;
    vn.Z = this->Z + v.Z;
    return vn;
}

Vector Vector::operator-(const Vector& v) const
{
    Vector vn;
    vn.X = this->X - v.X;
    vn.Y = this->Y - v.Y;
    vn.Z = this->Z - v.Z;
    return vn;
}

Vector Vector::operator*(float c) const
{
    return Vector(this->X * c, this->Y * c, this->Z * c);
}



Vector Vector::operator-() const
{
    return Vector(((-1 * this->X)), ((-1 * this->Y)), ((-1 * this->Z)));
}

Vector& Vector::operator+=(const Vector& v)
{
    *this = *this + v;
	return *this;
}

Vector Vector::normalized()
{
    if(abs(this->X)<EPSILON && abs(this->Y)<EPSILON && abs(this->Z)<EPSILON)
        return Vector(0,0,0);
    Vector v = *this * (1/this->length());;
    return v;
}



Vector& Vector::normalize()
{
    *this = *this * (1/this->length());
	return *this;
}

float Vector::length() const
{
    return sqrt(this->lengthSquared());
}

float Vector::lengthSquared() const
{
    return this->X*this->X+this->Y*this->Y+this->Z*this->Z;
}

Vector Vector::reflection( const Vector& normal) const
{
    Vector l = (*this);
    l.normalize();
    Vector vn = l - (normal * 2.0f) * (normal.dot(l));

	return vn;
}

bool Vector::triangleIntersection( const Vector& d, const Vector& a, const Vector& b, const Vector& c, float& s) const
{
    //Rechte Handregel, damit n in die korrekte Richtung zeigt!
    Vector n = (b - a).cross((c - a)).normalize();
    if(fabs(d.dot(n)) < EPSILON){
        //Lichtstrahl und Dreieck sind parallel
        return false;
    }
    //fd ist Referenzwert für jeden Punkt in der Ebene, da 'a' auf jeden Fall in der Ebene liegt
    float fd = n.dot(a);
    s = (fd - (n.dot(*this))) / (n.dot(d));
    //wenn s kleiner epsilon, dann zeigt Strahl in entgegengesetzte Richtung
    if(s < EPSILON){
       return false;
    }
    // Schnittpunkt P
    Vector p = *this + d * s;
    //Flächenvergleich
    float areaABC = Vector::areaTriangle(a, b, c);
    float areaABP = Vector::areaTriangle(a, b, p);
    float areaACP = Vector::areaTriangle(a, c, p);
    float areaBCP = Vector::areaTriangle(b, c, p);
    // --- --- ---
    return ((areaABC+EPSILON) >= (areaABP + areaACP + areaBCP));
}

float Vector::areaTriangle(const Vector& a, const Vector& b, const Vector& c){
    return ( ((b - a).cross((c - a))).length() / 2.0f );
}

std::string Vector::toString() const{
    std::string str;
    str.append(std::to_string(this->X));
    str.append(", ");
    str.append(std::to_string(this->Y));
    str.append(", ");
    str.append(std::to_string(this->Z));
    return str;
}
