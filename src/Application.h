#ifndef Application_hpp
#define Application_hpp

#include <stdio.h>
#include <list>
#include "camera.h"
#include "phongshader.h"
#include "constantshader.h"
#include "vertexbuffer.h"
#include "indexbuffer.h"
#include "basemodel.h"
#include "ShadowMapGenerator.h"
#include "Car.h"
#include "City.hpp"
#include "Track.hpp"


class Application
{
public:
    typedef std::list<BaseModel*> ModelList;
    Application(GLFWwindow* pWin);
    ~Application();
    void start();
    void update(float dtime);
    void draw();
    void end();
protected:
	void createScene();
    Camera Cam;
    ModelList Models;
    std::vector<City*> Districts;
    GLFWwindow* pWindow;
	BaseModel* pModel;
    Car* pCar;
    Track* pTrack;
    Model* pSky;
	ShadowMapGenerator ShadowGenerator;
};

#endif /* Application_hpp */
