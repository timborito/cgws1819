//
//  TriangleBoxModel.cpp
//  CGXcode
//
//  Created by Philipp Lensing on 10.10.16.
//  Copyright © 2016 Philipp Lensing. All rights reserved.
//

#include "TriangleBoxModel.h"

TriangleBoxModel::TriangleBoxModel(float Width, float Height, float Depth)
{
    Height = Height;
    const int planes = 6;
    float hW = Width/2;
    float hD = Depth/2;
    float hH = Height/2;
    
    //floor,cover,left,right,back,front
    
    Vector l_b_b = Vector(-hW,-hH,-hD);
    Vector r_b_b = Vector(hW,-hH,-hD);
    Vector r_b_f = Vector(hW,-hH,hD);
    Vector l_b_f = Vector(-hW,-hH,hD);

    Vector l_t_b = Vector(-hW,hH,-hD);
    Vector r_t_b = Vector(hW,hH,-hD);
    Vector r_t_f = Vector(hW,hH,hD);
    Vector l_t_f = Vector(-hW,hH,hD);
    
    
    Vector vbArr[ planes * 4] = {
        //floor
        l_b_b,
        r_b_b,
        r_b_f,
        l_b_f,
        //cover
        r_t_b,
        l_t_b,
        l_t_f,
        r_t_f,
        //left
        l_t_f,
        l_t_b,
        l_b_b,
        l_b_f,
        //right
        r_b_b,
        r_t_b,
        r_t_f,
        r_b_f,
        //back
        l_b_b,
        l_t_b,
        r_t_b,
        r_b_b,
        //front
        r_b_f,
        r_t_f,
        l_t_f,
        l_b_f,
    };
    
    

    GLfloat txArr[4*2*planes] = {
        //floor
        0,0,
        1,0,
        1,1,
        0,1,
        //cover
        0,0,
        1,0,
        1,1,
        0,1,
        //left
        1,0,
        0,0,
        0,1,
        1,1,
        //right
        1,1,
        1,0,
        0,0,
        0,1,
        //back
        1,1,
        1,0,
        0,0,
        0,1,
        //front
        1,2,
        1,0,
        0,0,
        0,2,
    };
    
    Vector normals[planes]{
        //floor
        Vector(0,-1,0),
        //cover
        Vector(0,1,0),
        //left
        Vector(-1,0,0),
        //right
        Vector(1,0,0),
        //back
        Vector(0,0,-1),
        //front
        Vector(0,0,1),
    };
    Vector nrmArr[4*planes];
    
    VB.begin();
    for(unsigned int i = 0;i<(planes*4);i++){
        VB.addNormal(normals[i/4]);
        VB.addTexcoord0(txArr[i*2],txArr[i*2+1]);
        VB.addVertex(vbArr[i]);
    }
    VB.end();
    
    GLint ibArr [planes*2*3] = {
        0,1,2,
        2,3,0,
    };
    
    for(unsigned int p = 1;p<planes;p++)
        for(unsigned int i = 0;i<3*2;i++)
            ibArr[p*planes+i] = ibArr[i]+4*p;
        
    
    
    
    IB.begin();
    for(unsigned int i = 0;i< (planes*3*2) ;i++){
        std::cout << ibArr[i] << std::endl;
        IB.addIndex(ibArr[i]);
    }
    IB.end();
}

void TriangleBoxModel::draw(const BaseCamera& Cam)
{
    BaseModel::draw(Cam);
    
    VB.activate();
    IB.activate();
    
    glDrawElements(GL_TRIANGLES, IB.indexCount(), IB.indexFormat(), 0);
    
    IB.deactivate();
    VB.deactivate();
}
