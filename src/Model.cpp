#include "Model.h"
#include "phongshader.h"
#include <list>
using namespace std;
#define FIT_SCALE 1.732f

Model::Model() : pMeshes(NULL), MeshCount(0), pMaterials(NULL), MaterialCount(0)
{

}
Model::Model(const char* ModelFile, bool FitSize) : pMeshes(NULL), MeshCount(0), pMaterials(NULL), MaterialCount(0)
{
    bool ret = this->load(ModelFile);
    if(!ret)
        throw std::exception();
}
Model::~Model()
{
    delete [] pMeshes;
    delete [] pMaterials;
    delete BoundingBox;
    deleteNodes(&RootNode);
}

void Model::deleteNodes(Node* pNode)
{
    if(!pNode)
        return;
    for(unsigned int i=0; i<pNode->ChildCount; ++i)
        deleteNodes(&(pNode->Children[i]));
    if(pNode->ChildCount>0)
        delete [] pNode->Children;
    if(pNode->MeshCount>0)
        delete [] pNode->Meshes;
}

bool Model::load(const char* ModelFile, bool FitSize)
{
    
    const aiScene* pScene = aiImportFile( ModelFile,aiProcessPreset_TargetRealtime_Fast | aiProcess_TransformUVCoords | aiProcess_FlipUVs);
    
    if(pScene==NULL || pScene->mNumMeshes<=0)
        return false;
    
    Filepath = ModelFile;
    Path = Filepath;
    size_t pos = Filepath.rfind('/');
    if(pos == std::string::npos)
        pos = Filepath.rfind('\\');
    if(pos !=std::string::npos)
        Path.resize(pos+1);
    
    loadNodes(pScene);
    loadMeshes(pScene, FitSize);
    loadMaterials(pScene);

    
    return true;
}

void Model::loadMeshes(const aiScene* pScene, bool FitSize)
{
    MeshCount = pScene->mNumMeshes;
    pMeshes = new Mesh[MeshCount];
    calcBoundingBox(pScene);

    for(unsigned int i=0;i<MeshCount;i++){
        //current mesh m
        Mesh& m = this->pMeshes[i];
        //fill vertex buffer
        m.MaterialIdx = pScene->mMeshes[i]->mMaterialIndex;
        m.VB.begin();

        for(unsigned int v = 0;v<pScene->mMeshes[i]->mNumVertices;v++){

            aiVector3D n = pScene->mMeshes[i]->mNormals[v];
            aiVector3D aV = pScene->mMeshes[i]->mVertices[v];
            Vector aVec = Vector(aV.x, aV.y, aV.z);
            m.VB.addNormal(n.x,n.y,n.z);
            if(pScene->mMeshes[i]->mTextureCoords[0]){
                aiVector3D tc = pScene->mMeshes[i]->mTextureCoords[0][v];
                m.VB.addTexcoord0(tc.x, tc.y);
            }
            
            if(pScene->mMeshes[i]->HasTangentsAndBitangents()){
                aiVector3D tang = pScene->mMeshes[i]->mTangents[v];
                m.VB.addTexcoord1(tang.x, tang.y, tang.z);
                aiVector3D biTang = pScene->mMeshes[i]->mBitangents[v];
                m.VB.addTexcoord2(biTang.x, biTang.y,biTang.z);
            }
             
            m.VB.addVertex(aVec);
        }
        m.VB.end();
        
        //fill index buffer
        m.IB.begin();
        for(unsigned int f = 0; f< pScene->mMeshes[i]->mNumFaces; f++){
            aiFace fa = pScene->mMeshes[i]->mFaces[f];
            for(unsigned int fi = 0; fi<fa.mNumIndices; fi++){
                m.IB.addIndex(fa.mIndices[fi]);
            }
        }
        m.IB.end();
    }
    
    float diagonal = (BoundingBox->Max - BoundingBox->Min ).length();
    
    if(FitSize && diagonal >= FIT_SCALE){
        Matrix mtr;
        mtr.scale((FIT_SCALE*3.0f)/diagonal);
        this->transform(mtr);
    }
}


bool has_suffix(const std::string &str, const std::string &suffix)
{
    return str.size() >= suffix.size() &&
    str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

void Model::loadMaterials(const aiScene* pScene)
{
    this->MaterialCount  = pScene->mNumMaterials;
    
    pMaterials = new Material[MaterialCount];
    
    for(unsigned int m = 0;m<MaterialCount;m++){
        aiMaterial* mat = pScene->mMaterials[m];
        aiColor3D cAmb;
        aiColor3D cDiff;
        aiColor3D cSpec;
        float exp;
        
        
        mat->Get(AI_MATKEY_COLOR_AMBIENT,cAmb);
        mat->Get(AI_MATKEY_COLOR_DIFFUSE,cDiff);
        mat->Get(AI_MATKEY_COLOR_SPECULAR,cSpec);
        mat->Get(AI_MATKEY_SHININESS,exp);
                
        
        
        pMaterials[m].AmbColor  = Color(cAmb.r,cAmb.g,cAmb.b);
        pMaterials[m].DiffColor = Color(cDiff.r,cDiff.g,cDiff.b);
        pMaterials[m].SpecColor = Color(cSpec.r,cSpec.g,cSpec.b);
        pMaterials[m].SpecExp = exp;
        
        
        if(mat->GetTextureCount(aiTextureType_DIFFUSE)){
            aiString path;
            mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
            aiString concat = aiString(Path);
            concat.Append(path.C_Str());
            pMaterials[m].DiffTex = Texture::LoadShared(concat.C_Str());
            
            //load normal-map
            std::string sPath = concat.C_Str();
            size_t lastindex = sPath.find_last_of(".");
            sPath.insert(lastindex,"_n");
            pMaterials[m].NormTex = Texture::LoadShared(sPath.c_str());
        }
    }
    
}
void Model::calcBoundingBox(const aiScene* pScene)
{
    aiVector3D aiVec = pScene->mMeshes[0]->mVertices[0];
    Vector vMin(Vector(aiVec.x,aiVec.y,aiVec.z));
    Vector vMax = Vector(vMin);
    
    for (int i = 0; i < pScene->mNumMeshes; i++) {
        for (int j = 0; j < pScene->mMeshes[i]->mNumVertices; j++) {
            aiVector3D aV = pScene->mMeshes[i]->mVertices[j];

            vMin.X = std::fmin(aV.x, vMin.X);
            vMin.Y = std::fmin(aV.y, vMin.Y);
            vMin.Z = std::fmin(aV.z, vMin.Z);
    
            vMax.X = std::fmax(aV.x, vMax.X);
            vMax.Y = std::fmax(aV.y, vMax.Y);
            vMax.Z = std::fmax(aV.z, vMax.Z);
        }
    }
    BoundingBox = new AABB(vMin,vMax,true);
    BoundingBox->setTransform(transform());
}

void Model::loadNodes(const aiScene* pScene)
{
    deleteNodes(&RootNode);
    copyNodesRecursive(pScene->mRootNode, &RootNode);
}

void Model::copyNodesRecursive(const aiNode* paiNode, Node* pNode)
{
    pNode->Name = paiNode->mName.C_Str();
    pNode->Trans = convert(paiNode->mTransformation);
    if(paiNode->mNumMeshes > 0)
    {
        pNode->MeshCount = paiNode->mNumMeshes;
        pNode->Meshes = new int[pNode->MeshCount];
        for(unsigned int i=0; i<pNode->MeshCount; ++i)
            pNode->Meshes[i] = (int)paiNode->mMeshes[i];
    }
    if(paiNode->mNumChildren <=0)
        return;
    
    pNode->ChildCount = paiNode->mNumChildren;
    pNode->Children = new Node[pNode->ChildCount];
    for(unsigned int i=0; i<paiNode->mNumChildren; ++i)
    {
        copyNodesRecursive(paiNode->mChildren[i], &(pNode->Children[i]));
        pNode->Children[i].Parent = pNode;
    }
}

void Model::applyMaterial( unsigned int index)
{
    if(index>=MaterialCount)
        return;
    
    PhongShader* pPhong = dynamic_cast<PhongShader*>(shader());
    if(!pPhong) {
        std::cout << "Model::applyMaterial(): WARNING Invalid shader-type. Please apply PhongShader for rendering models.\n";
        return;
    }
    
    Material* pMat = &pMaterials[index];
    pPhong->ambientColor(pMat->AmbColor);
    pPhong->diffuseColor(pMat->DiffColor);
    pPhong->specularExp(pMat->SpecExp);
    pPhong->specularColor(pMat->SpecColor);
    pPhong->diffuseTexture(pMat->DiffTex);
    pPhong->normalTexture(pMat->NormTex);
}

void Model::draw(const BaseCamera& Cam)
{
    if(!pShader) {
        std::cout << "BaseModel::draw() no shader found" << std::endl;
        return;
    }
    pShader->modelTransform(transform());
    BoundingBox->draw(Cam);
    std::list<Node*> DrawNodes;
    DrawNodes.push_back(&RootNode);
    
    while(!DrawNodes.empty())
    {
        Node* pNode = DrawNodes.front();
        
        if(pNode->Parent != NULL)
            pNode->GlobalTrans = pNode->Parent->GlobalTrans * pNode->Trans;
        else
            pNode->GlobalTrans = transform() * pNode->Trans;
        
        pShader->modelTransform(pNode->GlobalTrans);
        
        for(unsigned int i = 0; i<pNode->MeshCount; ++i )
        {
            Mesh& mesh = pMeshes[pNode->Meshes[i]];
            
            mesh.VB.activate();
            mesh.IB.activate();
            applyMaterial(mesh.MaterialIdx);
            pShader->activate(Cam);
            glDrawElements(GL_TRIANGLES, mesh.IB.indexCount(), mesh.IB.indexFormat(), 0);
            mesh.IB.deactivate();
            mesh.VB.deactivate();
        }
        for(unsigned int i = 0; i<pNode->ChildCount; ++i )
            DrawNodes.push_back(&(pNode->Children[i]));
        
        DrawNodes.pop_front();
    }
}

Matrix Model::convert(const aiMatrix4x4& m)
{
    return Matrix(m.a1, m.a2, m.a3, m.a4,
                  m.b1, m.b2, m.b3, m.b4,
                  m.c1, m.c2, m.c3, m.c4,
                  m.d1, m.d2, m.d3, m.d4);
}



