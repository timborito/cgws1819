#ifndef Track_hpp
#define Track_hpp

#include <stdio.h>
#include "Physic.hpp"
using namespace std;

class Track: public Model{
public:
    vector<Vector> WallVertices;
    vector<Vector> Axis;
    void identifyWall();
    Track();
};
#endif /* Track_hpp */
