#include "Aabb.h"

AABB AABB::UnitBox(Vector(-1,-1,-1), Vector(1,1,1));

AABB::AABB()
{
    Transform.identity();
}
AABB::AABB(const Vector& min, const Vector& max,bool draw) : Min(min), Max(max), Drawable(draw)
{
    
    Transform.identity();
    
    /*
     * BoundingBox kann angezeigt werden
     * Vertices werden mit Maxima berechnet und in den VertexBuffer uebergeben
     */
    
    if(Drawable){
        ConstantShader* c = new ConstantShader();
        c->color(Color(0,1,0));
        pShader = c;
        Vector v1,v2;
        VB.begin();
            //bottom
            v1 = Vector(Min.X, Min.Y, Min.Z);
            v2 = Vector(Max.X, Min.Y, Min.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v1 = Vector(Max.X, Min.Y, Min.Z);
            v2 = Vector(Max.X, Min.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v2 = Vector(Min.X, Min.Y, Max.Z);
            v1 = Vector(Max.X, Min.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v1 = Vector(Min.X, Min.Y, Min.Z);
            v2 = Vector(Min.X, Min.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            //top
        
            v1 = Vector(Min.X, Max.Y, Min.Z);
            v2 = Vector(Max.X, Max.Y, Min.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v1 = Vector(Min.X, Max.Y, Max.Z);
            v2 = Vector(Max.X, Max.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
            //front back edges
            v1 = Vector(Min.X, Max.Y, Min.Z);
            v2 = Vector(Min.X, Max.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v1 = Vector(Max.X, Max.Y, Min.Z);
            v2 = Vector(Max.X, Max.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            //poles
            v1 = Vector(Min.X, Min.Y, Min.Z);
            v2 = Vector(Min.X, Max.Y, Min.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v1 = Vector(Max.X, Min.Y, Min.Z);
            v2 = Vector(Max.X, Max.Y, Min.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v1 = Vector(Min.X, Min.Y, Max.Z);
            v2 = Vector(Min.X, Max.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
            v1 = Vector(Max.X, Min.Y, Max.Z);
            v2 = Vector(Max.X, Max.Y, Max.Z);
            VB.addVertex(v1);
            VB.addVertex(v2);
        
        VB.end();
        //Ausrichtungen der Seiten berechnen
        Faces = CalcFaces();
        
    }
}
AABB::AABB(float minX, float minY, float minZ, float maxX, float maxY, float maxZ,bool draw) : Min(minX, minY, minZ), Max(maxX, maxY, maxZ), Drawable(draw)
{
    Transform.identity();
    
}

Vector AABB::size() const
{
    return Max-Min;
}


AABB AABB::transform(const Matrix& m) const
{
    Vector c[8];
    corners(c);
    for (int i = 0; i < 8; ++i)
        c[i] = m * c[i];
    AABB r;
    r.fromPoints(c, 8);
    return r;
}

AABB AABB::merge(const AABB& a, const AABB& b) const
{
    AABB r;
    
    r.Min.X = a.Min.X < b.Min.X ? a.Min.X : b.Min.X;
    r.Min.Y = a.Min.Y < b.Min.Y ? a.Min.Y : b.Min.Y;
    r.Min.Z = a.Min.Z < b.Min.Z ? a.Min.Z : b.Min.Z;
    
    r.Max.X = a.Max.X > b.Max.X ? a.Max.X : b.Max.X;
    r.Max.Y = a.Max.Y > b.Max.Y ? a.Max.Y : b.Max.Y;
    r.Max.Z = a.Max.Z > b.Max.Z ? a.Max.Z : b.Max.Z;
    
    return r;
}

AABB& AABB::merge(const AABB& a)
{
    Min.X = a.Min.X < Min.X ? a.Min.X : Min.X;
    Min.Y = a.Min.Y < Min.Y ? a.Min.Y : Min.Y;
    Min.Z = a.Min.Z < Min.Z ? a.Min.Z : Min.Z;
    
    Max.X = a.Max.X > Max.X ? a.Max.X : Max.X;
    Max.Y = a.Max.Y > Max.Y ? a.Max.Y : Max.Y;
    Max.Z = a.Max.Z > Max.Z ? a.Max.Z : Max.Z;
    
    return *this;
}

void AABB::corners(Vector c[8]) const
{
    c[0].X = Min.X; c[0].Y = Min.Y; c[0].Z = Min.Z;
    c[1].X = Max.X; c[1].Y = Min.Y; c[1].Z = Min.Z;
    c[2].X = Max.X; c[2].Y = Max.Y; c[2].Z = Min.Z;
    c[3].X = Min.X; c[3].Y = Max.Y; c[3].Z = Min.Z;
    
    c[4].X = Min.X; c[4].Y = Min.Y; c[4].Z = Max.Z;
    c[5].X = Max.X; c[5].Y = Min.Y; c[5].Z = Max.Z;
    c[6].X = Max.X; c[6].Y = Max.Y; c[6].Z = Max.Z;
    c[7].X = Min.X; c[7].Y = Max.Y; c[7].Z = Max.Z;
}

/*
 * gibt mit Model-Matrix transformierte Eckpunkte der Boundingbox zurueck
 */
vector<Vector> AABB::transformedCorners() const
{
    vector<Vector> result;
    Vector c[8];
    corners(c);
    for(unsigned int i=0;i<8;i++){
        result.push_back(Transform*c[i]);
    }
    return result;
}
vector<Vector> AABB::CalcFaces() const
{
 
    vector<Vector> edges;
    vector<Vector> faces;
    //Kanten berechnen
    //0
    edges.push_back(Vector(Max.X,Min.Y,Min.Z)-Vector(Min.X,Min.Y,Min.Z));
    //1
    edges.push_back(Vector(Max.X,Min.Y,Max.Z)-Vector(Max.X,Min.Y,Min.Z));
    //2
    edges.push_back(Vector(Min.X,Min.Y,Max.Z)-Vector(Max.X,Min.Y,Max.Z));
    //3
    edges.push_back(Vector(Min.X,Min.Y,Min.Z)-Vector(Min.X,Min.Y,Max.Z));
    //4
    edges.push_back(Vector(Min.X,Max.Y,Min.Z)-Vector(Min.X,Min.Y,Min.Z));

    
    
    //Seiten berechnen mittels Kanten
    //Boden und Deckel interessieren nicht
    
    //bottom
    //faces.push_back((edges[0].cross(edges[1])));
    //front
    faces.push_back((edges[0].cross(edges[4])));
    //left
    faces.push_back(((edges[1]).cross(edges[4])));
    //back
    faces.push_back((edges[2].cross(edges[4])));
    //right
    faces.push_back((edges[3].cross(edges[4])));
    //top
    //faces.push_back((edges[0].cross(edges[3])));
    return faces;
    
    
}
vector<Vector> AABB::GetAxis() const {
    vector<Vector> result;
    for(Vector f : Faces){
        //achsen mit Model-Matrix Transformieren
        result.push_back(Transform*f);
    }
    return result;
}

void AABB::fromPoints(const Vector* Points, unsigned int PointCount)
{
    Max = Vector(-1e20f, -1e20f, -1e20f);
    Min = Vector(1e20f, 1e20f, 1e20f);
    
    for (unsigned int i = 0; i < PointCount; ++i)
    {
        if (Min.X > Points[i].X) Min.X = Points[i].X;
        if (Min.Y > Points[i].Y) Min.Y = Points[i].Y;
        if (Min.Z > Points[i].Z) Min.Z = Points[i].Z;
        if (Max.X < Points[i].X) Max.X = Points[i].X;
        if (Max.Y < Points[i].Y) Max.Y = Points[i].Y;
        if (Max.Z < Points[i].Z) Max.Z = Points[i].Z;
    }
    
}

Vector AABB::center() const
{
    return (Max + Min)*0.5f;
}

const AABB& AABB::unitBox()
{
    return UnitBox;
}
void AABB::setTransform(const Matrix &m){
    Transform = m;
}

/*
 * Zeichnenfunktion
 */
void AABB::draw(const BaseCamera& Cam){
    return;
    if(!Drawable){
        return;
    }
    if(!pShader) {
        std::cout << "BaseModel::draw() no shader found" << std::endl;
        return;
    }
    
    pShader->modelTransform(transform());
    pShader->activate(Cam);
    
    if(VB.GetVertexCount()<=0){
        return;
    }
    VB.activate();
    
    glDrawArrays(GL_LINES, 0, VB.vertexCount());
    
    VB.deactivate();
}
