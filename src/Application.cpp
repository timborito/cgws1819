#include "Application.h"
#ifdef WIN32
#include <GL/glew.h>
#include <glfw/glfw3.h>


#define _USE_MATH_DEFINES
#include <math.h>
#else
#define GLFW_INCLUDE_GLCOREARB
#define GLFW_INCLUDE_GLEXT
#include <GLFW/glfw3.h>
#endif
#include "model.h"
#include "ShaderLightMapper.h"
#include "Car.h"
#include "City.hpp"

#ifdef WIN32
#define ASSET_DIRECTORY "../../assets/"
#else
#define ASSET_DIRECTORY "../assets/"
#endif


Application::Application(GLFWwindow* pWin) : pWindow(pWin), Cam(pWin), pModel(NULL), ShadowGenerator(2048, 2048)
{
	createScene();
}

Application::~Application(){
    for(BaseModel* b : this->Models){
        delete b;
    }
}

void Application::start()
{
    glEnable (GL_DEPTH_TEST); // enable depth-testing
    glDepthFunc (GL_LESS); // depth-testing interprets a smaller value as "closer"
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

/*
 *  Steuerung updaten
 *  Kraefte berechnen
 *  Positionen updaten
 */
void Application::update(float dtime)
{
    float xAxis = 0;
    float yAxis = 0;
    int Break =  0;
    
    //Hardwareinput
    if(glfwGetKey(pWindow,GLFW_KEY_LEFT)){
        xAxis = -1;
    }
    if(glfwGetKey(pWindow,GLFW_KEY_RIGHT)){
        xAxis = 1;
    }
    if(glfwGetKey(pWindow,GLFW_KEY_UP)){
        yAxis = -1;
    }
    if(glfwGetKey(pWindow,GLFW_KEY_DOWN)){
        yAxis = 1;
    }
    if(glfwGetKey(pWindow,GLFW_KEY_SPACE)){
        Break = 1;
    }
    
    /*
    //Debug - Auto zuruecksetzen
    if(glfwGetKey(pWindow,GLFW_KEY_R)){
        Matrix t;
        t.identity();
        pCar->transform(t);
        pCar->SetPosition(Vector());
        pCar->SetRotation(Vector());
    }*/
    
    //setze Steuerung für Auto
    pCar->controls(yAxis, xAxis,Break);
    //DEBUG BoundingBox-Farbe aendern
    pCar->SetGridColor(Color(0,1,0));
    
    //Kollisionstest Auto vesus Strecke
    int idx = -1;
    bool carCollide = false;
    //Teste Kollision mit allen Wand-Vertizes
    for(unsigned int w = 0; w<pTrack->WallVertices.size();w+=3){
        idx++;
        vector<Vector> TriVertices = vector<Vector>();
        TriVertices.push_back(pTrack->WallVertices[w]);
        TriVertices.push_back(pTrack->WallVertices[w+1]);
        TriVertices.push_back(pTrack->WallVertices[w+2]);
        
        //teste ob Wand-Dreieck im Radius von 150 ist
        float minDist = INFINITY;
        for(Vector v : TriVertices){
            minDist = fminf(minDist,(v-pCar->GetPosition()).length());
        }
        //überspringen falls nicht
        if(minDist>=150.0f){
            continue;
        }
        
        //Zwischenspeicherung der Kollisions-Achsen des Dreiecks
        vector<Vector> WallAxis;
        int aIdx = idx*5;
        WallAxis.push_back(pTrack->Axis[aIdx]);
        WallAxis.push_back(pTrack->Axis[aIdx+1]);
        WallAxis.push_back(pTrack->Axis[aIdx+2]);
        WallAxis.push_back(pTrack->Axis[aIdx+3]);
        WallAxis.push_back(pTrack->Axis[aIdx+4]);

        //nahste Achse zur Kollision
        Vector oAxis = Vector(INFINITY,INFINITY,INFINITY);
        //Faktor Kollisionstiefe
        float oFactor = INFINITY;
        //relationale Geschwindigkeit der zu testenden Objekte (Dreieck steht still)
        Vector rel = pCar->Velocity;
        //kollisions-test
        Physic::Intersection iSec = Physic::getInstance()->satCollision(pCar->boundingBox().GetAxis(), pCar->boundingBox().transformedCorners(), WallAxis, TriVertices, oFactor, oAxis, rel,dtime);
        oAxis.normalize();
        
        
        if(iSec.Intersect || iSec.WillIntersect){
            carCollide = true;
            //Y Komponente tilgen
            oAxis.Y = 0;
            //Auto hart aus der Kollision drücken
            Vector push = oAxis*-oFactor;
            pCar->SetPosition(pCar->GetPosition() + push);
            //Geschwindigkeit an Kollisionsachse spiegeln und reduzieren
            pCar->Velocity = pCar->Velocity + ((oAxis * oAxis.dot(pCar->Velocity))*-2.0f) * 0.8f;
            //DEBUG BoundingBox des Autos faerben
            pCar->SetGridColor(Color(1,0,0));
        }
    }
    //Kollision setzen
    pCar->Collide = carCollide;
    
    //omega=Lenkung
    float omega = 0.0f;
    pCar->update(dtime,omega);
    //Kamera vor Auto ausrichten mit Verschiebung
    Cam.setTarget(pCar->GetPosition() + (pCar->transform().forward()*6.0f) + pCar->transform().right()*(2.0f*Cam.shift().X));
    //Faktor für Geschwindigkeit
    float velFactor = 1.0f+pCar->Velocity.length()*0.1f;
    //Vektor für Kameraposition in Abhaengigkeit der Geschwindkigkeit des Autos
    Vector behindVel = (pCar->transform().forward()*3.0f + pCar->transform().up()*-2.0f )* (1.0f/velFactor);
    Vector behind = pCar->transform().forward()*6.0f + behindVel;

    //Kraft fuer horizontalen Kameraschwenk ausueben
    Cam.addPanForce(Vector(-omega*3.0f,0,0),dtime);
    //Kameraposition aendern
    Cam.setPosition((pCar->GetPosition()-behind)+Vector(0,3.0f,0));
    
    //Skybox mit Kamera verschieben
    Matrix transSky,sky,scl;
    sky =pSky->transform();
    sky.m03 = 0;
    sky.m13 = 0;
    sky.m23 = 0;
    transSky.translation(Cam.position());
    pSky->transform(transSky * sky);
    Cam.update();
    //Haeuserblocks verschieben relativ zur Kamera
    for(City* c : Districts){
        c->updateTransform(Cam);
    }

}

void Application::draw()
{
	ShadowGenerator.generate(Models);
    // 1. clear screen
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	ShaderLightMapper::instance().activate();
    // 2. setup shaders and draw models
    for( ModelList::iterator it = Models.begin(); it != Models.end(); ++it )
    {
        (*it)->draw(Cam);
    }
	ShaderLightMapper::instance().deactivate();
    // 3. check once per frame for opengl errors
    GLenum Error = glGetError();
    assert(Error==0);
}
void Application::end()
{
    for( ModelList::iterator it = Models.begin(); it != Models.end(); ++it )
        delete *it;
    Models.clear();
}
/*
 * Szene mit Objekten fuellen
 */
void Application::createScene()
{
	//Skybox
    pSky = new Model(ASSET_DIRECTORY "skybox.obj", true);
    pSky->shader(new PhongShader(true,false), true);
    Matrix scl;
    scl.scale(350);
    pSky->transform(pSky->transform()*scl);
    pSky->shadowCaster(false);
    Models.push_back(pSky);

    //Auto
    pCar = new Car();
    pCar->loadModel(ASSET_DIRECTORY "MoonShadow/MoonShadow.obj");
    pCar->shader(new PhongShader(), true);
    pCar->shadowCaster(false);
    Models.push_back(pCar); 
    Physic::getInstance()->addObject(pCar);
    
    //Strecke
    pTrack = new Track();
    pTrack->load(ASSET_DIRECTORY "Strecke.dae",false);
    pTrack->shader(new PhongShader(), true);
    pTrack->shadowCaster(false);
    //Wand-Vertizes identifizieren
    pTrack->identifyWall();
    Models.push_back(pTrack);

    
    //Haeuserblocks hinzufuegen
    Vector cityPosOrg = Vector(0,-230,0);
    Vector cityPos,cityPos2;
    Matrix cityTrans,cityScl;
    for(int r=-2;r<=2;r++){
        cityPos = Vector((438.0f*r), cityPosOrg.Y , cityPosOrg.Z );
        for(int c=-2;c<=2;c++){
            City* pCity = new City();
            pCity->loadModel(ASSET_DIRECTORY"City.dae");
            pCity->shader(new PhongShader(), true);
            pCity->shadowCaster(false);
            cityPos2 = Vector(cityPos.X , cityPos.Y , (438.0f*c));
            pCity->SetPosition(cityPos2);
            pCity->SetScale(Vector(1,1,1)*31.0f);
            Models.push_back(pCity);
            Districts.push_back(pCity);
        }
    }
    
    //Direktionales Licht
    DirectionalLight* dl = new DirectionalLight();
	dl->direction(Vector(-1, -1, 0));
	dl->color(Color(0.58, 0.62, 0.6));
	dl->castShadows(true);
	ShaderLightMapper::instance().addLight(dl);
}
