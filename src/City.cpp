#include "City.hpp"

City::City(){

}
bool City::loadModel(const char* CityFile){
    
    load(CityFile);
    
    return true;
}

void City::updateTransform(Camera cam)
{

    Matrix scale,trans,rotX,rotY,rotZ,transBox;
    trans = transform();
    //Boundingbox zentireren

    Vector bCenter = BoundingBox->size()*-0.5f;
    bCenter.Y = 0;
    bCenter.Z *= 2;
    transBox.translation(bCenter);
    //Rotationen uebernehmen

    rotX.rotationX(Rotation.X);
    rotY.rotationY(Rotation.Y);
    rotZ.rotationZ(Rotation.Z);
    //Verschieben der eignen Position um Kamera position
    trans.translation(Position + cam.position());
    scale.scale(Scale);
    //Transformationen anwenden
    Matrix result = trans * rotY * rotZ * rotX * scale;
    BoundingBox->setTransform(result);
    transform(result);

}
