#ifndef __SimpleRayTracer__vector__
#define __SimpleRayTracer__vector__

#include <iostream>
#include <math.h>

class Vector
{
public:
    float X;
    float Y;
    float Z;
    
    Vector( float x, float y, float z);
    Vector();
    Vector(const Vector &v);
    
    float dot(const Vector& v) const;
    Vector cross(const Vector& v) const;
    Vector operator+(const Vector& v) const;
    Vector operator-(const Vector& v) const;
    Vector& operator+=(const Vector& v);
    Vector operator*(float c) const;
    Vector operator-() const;
    Vector& normalize();
    Vector normalized();
    bool equals(const Vector& v) const;
    float length() const;
    float lengthSquared() const;
    Vector perpendicular(){
        return Vector(-(this->Y),0,this->X);
    }
    
    
    
    Vector reflection( const Vector& normal) const;
    
    static void minimum(Vector& min, const Vector test){
        min.X = fminf(min.X, test.X);
        min.Y = fminf(min.Y,test.Y);
        min.Z = fminf(min.Z,test.Z);
    };
    
    static void maximum(Vector& max, const Vector test){
        max.X = fmaxf(max.X,test.X);
        max.Y = fmaxf(max.Y,test.Y);
        max.Z = fmaxf(max.Z,test.Z);
    };
    
    static void setMinimum(Vector min,Vector& v){
        v.X = fmaxf(min.X, v.X);
        v.Y = fmaxf(min.Y,v.Y);
        v.Z = fmaxf(min.Z,v.Z);
    };
    
    static void setMaximum(Vector max,Vector& v){
        v.X = fminf(max.X,v.X);
        v.Y = fminf(max.Y,v.Y);
        v.Z = fminf(max.Z,v.Z);
    };
    bool triangleIntersection( const Vector& d, const Vector& a, const Vector& b,
                              const Vector& c, float& s) const;
    
    static float areaTriangle(const Vector& a, const Vector& b, const Vector& c);
    std::string toString() const;
 };

#endif 
