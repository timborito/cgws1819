#ifndef Aabb_hpp
#define Aabb_hpp

#include <stdio.h>
#include "vector.h"
#include "matrix.h"
#include "VertexBuffer.h"
#include "Camera.h"
#include "BaseShader.h"
#include "ConstantShader.h"
using namespace std;

class AABB
{
public:

    Vector Min;
    Vector Max;
    AABB();
    AABB(const Vector& min, const Vector& max, bool draw=false);
    AABB(float minX, float minY, float minZ, float maxX, float maxY, float maxZ,bool draw=false);
    virtual void draw(const BaseCamera& Cam);
    Vector size() const;
	AABB transform(const Matrix& m) const;
    
    void setTransform(const Matrix& m);
    Matrix& transform(){ return Transform; }
	AABB merge(const AABB& a, const AABB& b) const;
	AABB& merge(const AABB& a);
	Vector center() const;
	void corners(Vector c[8]) const;
    vector<Vector> transformedCorners() const;
    vector<Vector> CalcFaces() const;
    vector<Vector> GetAxis()const;
	void fromPoints(const Vector* Points, unsigned int PointCount);
    static const AABB& unitBox();
    void SetGridColor(Color c){
        pShader->color(c);
    };
    
protected:
    static AABB UnitBox;
    bool Drawable = false;
    ConstantShader* pShader;
    Matrix Transform;
    VertexBuffer VB;
    vector<Vector> Faces;
    Color cGrid = Color(0,1,0);

};



#endif /* Aabb_hpp */
