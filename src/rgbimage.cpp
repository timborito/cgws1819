#include "rgbimage.h"
#include "color.h"
#include "assert.h"
#include "math.h"

RGBImage::RGBImage( unsigned int Width, unsigned int Height)
{
    this->m_Width = Width;
    this->m_Height = Height;
    Color (*image) = new Color[Width*Height];
    this->m_Image = image;
}

RGBImage::~RGBImage()
{
    if(this->m_Image!=NULL){
        delete this->m_Image;
    }
    
}

void RGBImage::setPixelColor( unsigned int x, unsigned int y, const Color& c)
{
    if( x < this->width() && y < this->height()){
        this->m_Image[(x*this->height() + y)] = c;
    }
}

const Color& RGBImage::getPixelColor( unsigned int x, unsigned int y) const
{
	return this->m_Image[x*this->height()+y];
}

unsigned int RGBImage::width() const
{
    return this->m_Width;
}
unsigned int RGBImage::height() const
{
    return this->m_Height;
}

unsigned char RGBImage::convertColorChannel( float v)
{
    
	if( v <= 0.0f)
        v = 0.0f;
    if(v>=1.0f){
        v = 1.0f;
    }
    
   return v*255.0f;
}


bool RGBImage::saveToDisk( const char* Filename)
{
    BFH bmp_head;
    BIH bmp_info;
    int size = this->width() * this->height() * 4;
    
    //file header
    bmp_head.bfType = 0x4D42; // 'BM'
    bmp_head.bfSize= size + sizeof(BFH) + sizeof(BIH);
    bmp_head.bfReserved1 = bmp_head.bfReserved2 = 0;
    bmp_head.bfOffBits = bmp_head.bfSize - size;
    
    //info header
    bmp_info.biSize = sizeof(BIH);
    bmp_info.biWidth = this->width();
    bmp_info.biHeight = this->height();
    bmp_info.biPlanes = 1;
    bmp_info.biBitCount = 24;
    bmp_info.biCompress = 0;
    bmp_info.biSizeImage = size;
    bmp_info.biXPelsPerMeter = 0;
    bmp_info.biYPelsPerMeter = 0;
    bmp_info.biClrUsed = 0 ;
    bmp_info.biClrImportant = 0;
    // finish the initial of infohead;
    
    FILE *file;
    if (!(file = fopen(Filename,"wb"))) return 0;
    //header schreiben
    fwrite(&bmp_head, 1, sizeof(BFH), file);
    fwrite(&bmp_info, 1, sizeof(BIH), file);
    

    
    unsigned int y = (this->m_Height);
    while(y>0){
        y--;
        unsigned int x = 0;
        while(x<this->m_Width){
            
            unsigned char r,g,b;
            r = RGBImage::convertColorChannel(this->m_Image[x*this->height()+y].R);
            g = RGBImage::convertColorChannel(this->m_Image[x*this->height()+y].G);
            b = RGBImage::convertColorChannel(this->m_Image[x*this->height()+y].B);
            fwrite(&b, 1, 1, file);
            fwrite(&g, 1, 1, file);
            fwrite(&r, 1, 1, file);
       x++;
        }
    }
    
    
    fclose(file);
	return true;

}

RGBImage & RGBImage::SobelFilter(RGBImage & dst, const RGBImage & src, float factor) {
    
    if(!(src.height() == dst.height() && src.width() == dst.width())){
        return dst;
    }
    
    float filter[3][3] = {
        {1.0f, 0.0f, -1.0f},
        {2.0f, 0.0f, -2.0f},
        {1.0f, 0.0f, -1.0F}
    };
    
    for (int w = 1; w < src.height() - 2; w++) {
        for (int h = 1; h < src.width() - 2; h++) {
            float x = 0;
            float y = 0;
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    float c = src.getPixelColor(w+i,h+j).R;
                    x += c * filter[i+1][j+1];
                    y += c * filter[j+1][i+1];
                }
            }
            float s = sqrt(x*x + y*y) * factor;
            dst.setPixelColor(w, h, Color(s, s, s));
        }
    }
    
    return dst;
}
