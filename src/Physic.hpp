
#ifndef Physic_hpp
#define Physic_hpp

#include <stdio.h>
#include "Aabb.h"
#include <vector>
#include "Model.h"



    
class PhysicObject : public Model{
    protected:
    
        Vector Rotation;
        Vector AngularAcceleration;
        Vector AngularVelocity;
        Vector Position;
        Vector BodyVelocity;
        Vector BodyAcceleration;
        Vector Scale = Vector(1,1,1);
    
        float Mass = 10.0f;
        bool IsRigidBody = false;
        std::vector<Vector>*Forces;
        std::vector<Vector>*BodyForces;
        std::vector<Vector>*AngularForces;
    public:
        Vector Acceleration;
        Vector Velocity;
        PhysicObject(bool IsRigidBody = false);
        void addForce(Vector fn){Forces->push_back(fn);};
        void addBodyForce(Vector fn){BodyForces->push_back(fn);};
        void addAngularForce(Vector af){AngularForces->push_back(af);};
        void applyForces(float dtime);
        Vector GetPosition(){return Position;};
        Vector GetGlobalPostion(){return Transform*Position;};
        void SetPosition(Vector p){Position = p;};
        void SetRotation(Vector r){Rotation = r;};
        void SetScale(Vector s){Scale = s;};
        bool IsStatic(){return !IsRigidBody;};
        virtual void simulate(float dtime);
        virtual void updateTransform();

    };



class Physic{
 
    static Physic *instance;
    private:
        std::vector<PhysicObject*> Objects;
    public:
        struct Projection{
            float Min;
            float Max;
        };
        struct Intersection{
            bool WillIntersect = true;
            bool Intersect = true;
        };
        Physic();
        static Physic *getInstance() {
            if(!instance)
                instance = new Physic();
            return instance;
        }
        void addObject(PhysicObject* obj){Objects.push_back(obj);};
        void collisions(PhysicObject *obj,float dtime);
    
        Projection projectVerticesOntoAxis(vector<Vector> vertices,Vector axis);
        Physic::Intersection satCollision(vector<Vector> axisA,vector<Vector> verticesA,vector<Vector> axisB,vector<Vector> verticesB,float &oFactor, Vector &oAxis,Vector relVelocity,float dtime);
        bool overlaps(Physic::Projection a,Physic::Projection b);
        float getOverlap(Physic::Projection a, Physic::Projection b);


    
        unsigned long getNumObjects(){return Objects.size();};
};

#endif /* Physic_hpp */
