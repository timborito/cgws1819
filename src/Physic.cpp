#include "Physic.hpp"

using namespace std;

Physic* Physic::instance = 0;

//Singelton
Physic::Physic(){
    Objects = std::vector<PhysicObject*>();
}


//Kollision von objA mit allen Physik Objekten testen
void Physic::collisions(PhysicObject * objA,float dtime){
    for(PhysicObject *objB : Objects){
        if(objA == objB)continue;
        //Kollisionsachsen Faktor
        float oFactor = INFINITY;
        //Kollisionsachse
        Vector oAxis;
        //Kollisionsachse von objA
        vector<Vector> axisA = objA->boundingBox().GetAxis();
        //Vertizes von objA
        vector<Vector> verticesA = objA->boundingBox().transformedCorners();
        //Kollisionsachse von objB
        vector<Vector> axisB = objB->boundingBox().GetAxis();
        //Vertizes von objB
        vector<Vector> verticesB = objB->boundingBox().transformedCorners();
        //Geschwindigkeitsdifferenz
        Vector vAB = objA->Velocity - objB->Velocity;
        //SAT Kollisionstest
        Physic::Intersection iSec = Physic::getInstance()->satCollision(axisA, verticesA, axisB, verticesB, oFactor, oAxis,vAB, dtime);
        oAxis.normalize();
        if(iSec.Intersect || iSec.WillIntersect){
            //Reflektiere geschwindigkeit 
            objA->Velocity = (objA->Velocity.normalized().reflection(oAxis)*objA->Velocity.length());
            //DEBUG BoundigBox faerben
            objA->SetGridColor(Color(1,0,0));
        }else{
            //DEBUG BoundigBox faerben
            objA->SetGridColor(Color(0,1,0));
        }
    }
}

/*
 * Kollision nach dem Separating Axis Theorem
 */
Physic::Intersection Physic::satCollision(vector<Vector> axisA, vector<Vector> verticesA,vector<Vector> axisB,vector<Vector> verticesB, float &oFactor, Vector &oAxis, Vector relVelocity,float dtime)
    {
    //Kollisionsachsen beider zutestenden Objekte zusammenfassen
    vector<Vector> bothAxis[2] = {axisA,axisB};
    //Struct fuer Kollisionsergebnis
    Physic::Intersection iSec;
    
    for(unsigned int index = 0;index<=1;index++){
        //Testachse a
        for(Vector a : bothAxis[index]){
            a.normalize();
            float dist = INFINITY;
            //Projektion der Vertizies beider Objekte auf Testachse
            //Physik:Projection beinhaltet Maxima der Projektionen
            Physic::Projection p1 = projectVerticesOntoAxis(verticesA,a);
            Physic::Projection p2 = projectVerticesOntoAxis(verticesB,a);
            //Schnitt der Max berechnen
            dist = getOverlap(p1,p2);
            //wenn minimalster Schnitt größer als 0 dann ist kein Schnitt vorhanden
            if(dist>=0)
                iSec.Intersect = false;
            
            //Berechnen ob Schnitt stattfinden wird in Zukunft
            float velocityProjection = a.dot(relVelocity);
            if (velocityProjection < 0) {
                p1.Min += velocityProjection *dtime;
            } else {
                p1.Max += velocityProjection *dtime;
            }
            dist = getOverlap(p1, p2);
            if(dist>=0)
                iSec.WillIntersect = false;
                dist = abs(dist);
        
            //Nur  minimalste Achse von Objekt B zurueck geben
            if(index==1 &&  dist < oFactor ){
                oFactor = dist;
                oAxis = a;
            }
        }//end for loop bothAxis[a]
    }//end for loop bothAxis
    //Schnitt zurueck geben
    return iSec;

}

/*
 *  Projektion von Vertizes vTest auf Achse axis
 */
Physic::Projection Physic::projectVerticesOntoAxis(vector<Vector> vTest,Vector axis){
    Physic::Projection pr;
    pr.Min = axis.dot(vTest[0]);
    pr.Max = pr.Min;
    for(unsigned int i=1;i<vTest.size();i++){
        //Projektion der Vertizes auf Achse 'axis'
        float p = axis.dot(vTest[i]);
        //Minima und Maxima feststelllen
        if(p<pr.Min)
            pr.Min = p;
        if(p>pr.Max)
            pr.Max = p;
    }
    return pr;
}

/*
 * Schnitt der Projektionen berchnen
 */
float Physic::getOverlap(Physic::Projection a,Physic::Projection b)
{
    return (a.Min < b.Min) ? (b.Min - a.Max) : (a.Min - b.Max);
}


PhysicObject::PhysicObject(bool IsRigidBody){
    Forces = new std::vector<Vector>();
    AngularForces = new std::vector<Vector>();
    BodyForces = new std::vector<Vector>();
    Velocity = Vector();
    Acceleration = Vector();
}

/*
 * Transformation updaten
 */
void PhysicObject::updateTransform(){
    Matrix scale,trans,rotX,rotY,rotZ,transBox;
    trans = transform();
    //Boundingbox zentireren
    Vector bCenter = BoundingBox->size()*-0.5f;
    bCenter.Y = 0;
    bCenter.Z *= 2;
    transBox.translation(bCenter);
    //Rotationen uebernehmen
    rotX.rotationX(Rotation.X);
    rotY.rotationY(Rotation.Y);
    rotZ.rotationZ(Rotation.Z);
    trans.translation(Position);
    //Skalieren
    scale.scale(Scale);
    //Transformationen anwenden
    Matrix result = trans * rotY * rotZ * rotX * scale;
    BoundingBox->setTransform(result);
    transform(result);
}

/*
 *  Physikalische Kraefte verrechnen
 */
void PhysicObject::simulate(float dtime){
    //Kollision mit anderne PhysicObjects berechnen
    Physic::getInstance()->collisions(this,dtime);
    //Kraefte auf Beschleunigungen verrechnen
    applyForces(dtime);
    //Beschleunigungen auf Geschwindigkeiten verrechnen
    Velocity += Acceleration * (1/Mass) * dtime;
    AngularVelocity += AngularAcceleration  * (1/Mass) * dtime;
    Position += Velocity * dtime;
    Rotation += AngularVelocity;
    //Transformationen updaten
    updateTransform();
}

/*
 *  Verrechnen hinzugefuegte Kreafte auf Beschleunigungen
 */
void PhysicObject::applyForces(float dtime)
{
    //Beschleunigungen zuruecksetzen
    Acceleration = Vector();
    AngularAcceleration = Vector();
    
    
    //Kraefte aus Kraefte-Vektoren verrechnen
    for(Vector f : *Forces){
        Forces->pop_back();
        Acceleration += f * dtime;
    }
    
    for(Vector f : *AngularForces){
        AngularForces->pop_back();
        AngularAcceleration += f * dtime;
    }

}
