#ifndef City_hpp
#define City_hpp

#include <stdio.h>
#include "Physic.hpp"

class City : public PhysicObject{
    public:
        City();
        bool loadModel(const char* ChassisFile);
        void updateTransform(Camera cam);
        
    
};
#endif /* City_hpp */
