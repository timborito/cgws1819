#include "Track.hpp"
#include <list>

Track::Track(){
    Matrix scl,tr,rot,rot2;
    rot.rotationY(M_PI*0.25f);
    rot2.rotationX(20);
    tr.translation(Vector(13,-4,0));
    scl.scale(0.6f);
    transform(transform()*tr*scl);
}

void Track::identifyWall(){
//  Node-List für das Model erstellen
    std::list<Model::Node *> DrawNodes;
//  Eltern-Node in die Liste
    DrawNodes.push_back(&RootNode);
    
    while(!DrawNodes.empty())
    {
//      Erstes Node-Element
        Node* pNode = DrawNodes.front();
        
//      falls Node nicht Eltern-Element, Transformation des Eltern-Nodes in pNode speichern
        if(pNode->Parent != NULL)
            pNode->GlobalTrans = pNode->Parent->GlobalTrans * pNode->Trans;
        else
            pNode->GlobalTrans = transform() * pNode->Trans;
        
//      Meshes der pNode durchgehen
        for(unsigned int i = 0; i<pNode->MeshCount; ++i )
        {
            Mesh& mesh = pMeshes[pNode->Meshes[i]];
            
            
                for(unsigned int idx = 0;idx<mesh.IB.indexCount();idx+=3){

//              Aus Vertizes Vektoren erstellen
                Vector a = mesh.VB.vertices()[mesh.IB.indices()[idx]];
                Vector b = mesh.VB.vertices()[mesh.IB.indices()[idx+1]];
                Vector c = mesh.VB.vertices()[mesh.IB.indices()[idx+2]];
                
//              Aus den Vektoren Dreiecke erstellen
                Vector ab = b-a;
                Vector ac = c-a;
                Vector cb = b-c;
            
//              Kreuprodukt berechnen für senkrechte aufliegenden Vektor eines Dreiecks
                Vector n  = ab.cross(ac);
                    
//              Prüfung ob Dreiecksnormale im rechten Winkel auf xz-Ebene sitzt
                float dProduct = n.normalized().dot(Vector(0,1,0));
                    
//              Falls (nahe) 90°, pNode transformieren und als Wand abspeichern
                if(abs(dProduct)<=0.01){
                    WallVertices.push_back(pNode->GlobalTrans*a);
                    WallVertices.push_back(pNode->GlobalTrans*b);
                    WallVertices.push_back(pNode->GlobalTrans*c);
//                  Achsen der Dreiecke transformieren und in Axis ablegen
                    Axis.push_back(pNode->GlobalTrans*n);
                    Axis.push_back(pNode->GlobalTrans*-n);
                    Axis.push_back(pNode->GlobalTrans*n.cross(ab));
                    Axis.push_back(pNode->GlobalTrans*n.cross(ac));
                    Axis.push_back(pNode->GlobalTrans*n.cross(cb));
                }
            }
        }
//      Solange Kinder-Nodes exisitieren, diese in DrawNodes zur Weiteren Transformation ablegen
        for(unsigned int i = 0; i<pNode->ChildCount; ++i )
            DrawNodes.push_back(&(pNode->Children[i]));
        
        DrawNodes.pop_front();
    }
    
    
}
