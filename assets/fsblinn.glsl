#version 400
in vec3 Position;
in vec3 Normal;
in vec2 Texcoord;

out vec4 FragColor;

uniform vec3 LightPos;
uniform vec3 LightColor;

uniform vec3 EyePos;
uniform vec3 DiffuseColor;
uniform vec3 SpecularColor;
uniform vec3 AmbientColor;
uniform float SpecularExp;
uniform float Time;
uniform sampler2D DiffuseTexture;

float sat( in float a)
{
    return clamp(a, 0.0, 1.0);
}

void main()
{
    vec3 SpecularComponent,SpecularComponentBlinn = vec3(0.0);
    vec4 DiffTex = texture( DiffuseTexture, Texcoord);
    if(DiffTex.a <0.3f) discard;
    vec3 N = normalize(Normal);
    vec3 L = normalize(LightPos-Position);
    vec3 E = normalize(EyePos-Position);
    vec3 DiffuseComponent = LightColor * DiffuseColor * sat(dot(N,L));
    vec3 R = reflect(-L,N);
    SpecularComponent = LightColor * SpecularColor * pow( sat(dot(R,L)), SpecularExp);
    
    if(dot(N, L) > 0){
        vec3 H = normalize(L+E);
        SpecularComponentBlinn = LightColor * SpecularColor * pow( max(dot(N, H), 0.0), SpecularExp);
    }
    //colorize blinn spec
    SpecularComponentBlinn.r *= 5.0f;

    if(sin(Time*5.0f)>0.0f){
        SpecularComponent = SpecularComponentBlinn;
    }else{
        //colorize phong spec
        SpecularComponent.g *= 5.0f;
    }
    
    FragColor = vec4((DiffuseComponent + AmbientColor)*DiffTex.rgb + SpecularComponent ,DiffTex.a);
}
