#version 400

#define POINT 0
#define DIRECTIONAL 1
#define SPOT 2
#define M_PI        3.14159265358979323846264338327950288   /* pi             */

in vec3 Position;
in vec3 Normal;
in vec2 Texcoord;

in vec3 Tangent;
in vec3 BiTangent;

out vec4 FragColor;

uniform vec3 LightPos;
uniform vec3 LightColor;

uniform vec3 EyePos;
uniform vec3 DiffuseColor;
uniform vec3 SpecularColor;
uniform vec3 AmbientColor;
uniform float SpecularExp;
uniform sampler2D DiffuseTexture;
uniform sampler2D NormalTexture;

const int MAX_LIGHTS=14;




struct Light
{
    int Type;
    vec3 Color;
    vec3 Position;
    vec3 Direction;
    vec3 Attenuation;
    vec3 SpotRadius;
    int ShadowIndex;
};

uniform Lights
{
    int LightCount;
    Light lights[MAX_LIGHTS];
};


float sat( in float a)
{
    return clamp(a, 0.0, 1.0);
}

float AttenuationCalc(vec3 Attenuation,float Distance){
    return (1.0f/ (Attenuation.x + Attenuation.y * Distance + Attenuation.z * (Distance * Distance) ));
}


vec3 lightCalc(vec3 Position, vec3 N, Light l, vec3 E,vec4 DiffTex){
    vec3 L;
    vec3 V = vec3(Position);
    vec3 P = vec3(l.Position);
    float Distance =  length(V-P);
    float Attenuation;
    switch (l.Type) {
        case POINT:
            L = l.Position-Position;
            Attenuation = AttenuationCalc(l.Attenuation,Distance);
            break;
            
        case DIRECTIONAL:
            L = - l.Direction;
            Attenuation = 1.0f;
            break;
            
        case SPOT:
            L = l.Position-Position;
            vec3 nD = l.Direction;
            vec3 nL = L;
            nD = normalize(nD);
            nL = normalize(nL);
            float sigma = acos(dot(-nD, nL));
            float relation = 1.0f - (sat((sigma - l.SpotRadius.x) / ( l.SpotRadius.y - l.SpotRadius.x)));
            Attenuation = AttenuationCalc(l.Attenuation,Distance) * relation;
            break;
        default:
            break;
    }

    
    //vec3 R = reflect(-L,N);
    vec3 H = normalize((E + L) / length(E + L));
    
    vec3 DiffuseComponent =   Attenuation * l.Color * DiffuseColor * sat(dot(N,L));
    vec3 SpecularComponent =  Attenuation * SpecularColor * l.Color * pow(max(0,dot(N,H)), SpecularExp);
    return vec3(   DiffuseComponent*DiffTex.rgb + SpecularComponent );
    //return vec3(   DiffuseComponent*DiffTex.rgb  );

    
    
}


void main()
{
    
    vec3 D = EyePos - Position;
    
    vec4 DiffTex = texture( DiffuseTexture, Texcoord);
    vec4 NormTex = texture( NormalTexture, Texcoord);
    if(DiffTex.a <0.3f) discard;
    
    mat3 NormMat = mat3(Tangent,BiTangent,Normal);
    vec3 mappedNorm =  Normal;
    vec3 xyzNorm  = vec3(NormTex.r*2.0-1.0,NormTex.g*2.0-1.0,NormTex.b*2.0-1.0);
//    if(NormTex.rgb != vec3(0.0)){
        mappedNorm =  (NormMat *xyzNorm).xyz;
//    }
    
    //vec3 N = normalize(Normal);
    vec3 N = normalize(mappedNorm);
    vec3 E = normalize(EyePos-Position);
    vec3 LightColor = vec3(0,0,0);
    
    for( int i=0;i<LightCount;i++){
        Light l = lights[i];
        LightColor += lightCalc(Position,N,l,E,DiffTex);
    
    }
    
    
    
    
    float dist = length(D);
    
    vec3 fogColor = vec3(0.2,0.2,0.2);
    
    float dmin= 10.0;
    float dmax= 550.0;
    float a= 3.0;
    float s = (dist-dmin)/(dmax-dmin);
    
    if(s <= 0){
        s=0.0;
    }else if(s>=1){
        s=1.0;
    }else{
        s= pow(s,a);
    }
    
    

   // vec4 mlColor = vec4( AmbientColor*DiffTex.rgb + LightColor,DiffTex.a);
    vec3 ColorSum = vec3( AmbientColor*DiffTex.rgb + LightColor);
    FragColor = vec4(ColorSum,DiffTex.a-s);
}
