
#ifndef Tank_hpp
#define Tank_hpp

#include <stdio.h>
#include "model.h"
#include "Physic.hpp"




class Car : public PhysicObject
{
public:
    Car();
    virtual ~Car();
    bool loadModel(const char* ChassisFile);
    void controls( float ForwardBackward, float LeftRight, unsigned int Break);
    void update(float dtime, float& omega);
    float ForwardBackward;
    float LeftRight;
    unsigned int Brake;
    int Reverse;
    bool Collide = false;
    
protected:
    Vector Direction;
    void updateTransform() override;
    void simulate(float dtime) override;
};



#endif
