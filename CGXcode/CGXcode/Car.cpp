#include "Car.h"
#include "AABB.h"


#define EPSILON 1e-5
#define DRAG 0.4f
#define BRAKE 1500.0f
#define ROLL_RESISTANCE 150.0f
using namespace std;


Car::Car(){

    this->pShader = NULL;
    this->DeleteShader = NULL;
    this->ForwardBackward = 0;
    this->LeftRight = 0;
    this->IsRigidBody = true;
    this->Mass = 10.0f;
    this->SetRotation(Vector(0,-0.5f,0));
    this->Position = Vector(59,-0.2,-85.2);

}

Car::~Car(){

}

/*
 *  Model laden
 */
bool Car::loadModel(const char* ChassisFile){
    load(ChassisFile);
    Direction = transform().forward();
    return true;
}
/*
 *  Steuerung updaten
 */
void Car::controls(float ForwardBackward, float LeftRight, unsigned int Brake){
    this->ForwardBackward = -ForwardBackward;
    this->LeftRight = -LeftRight;
    this->Brake = Brake;
    this->Reverse = (Velocity.dot(Direction)<0.0f)?-1:1;

}
/*
 *  Physik simulieren
 */
void Car::simulate(float dtime){
    
    Matrix rotY;
    //Kreafte auf Beschleunigungen verrechnen
    applyForces(dtime);
    //Beschleunigung auf Geschwindigkeit anwenden (Rotationsgeschwindigkeit)
    AngularVelocity += AngularAcceleration  * (1.0f/Mass) * dtime;
    
    //Drehgeschwindigkeit regulieren
    if(AngularVelocity.Y >(2.0f*M_PI))
        AngularVelocity.Y -= 2.0f*M_PI;
    if(AngularVelocity.Y <(-2.0f*M_PI))
        AngularVelocity.Y += 2.0f*M_PI;
    AngularVelocity.Z =  fmaxf(-M_PI_4,AngularVelocity.Z);
    AngularVelocity.Z =  fminf(M_PI_4,AngularVelocity.Z);
    
    //Drehgeschwindigkeit
    Rotation += AngularVelocity *dtime;
    
    //Rotationen beschraenken
    if(Rotation.Y >(2.0f*M_PI))
        Rotation.Y -= 2.0f*M_PI;
    if(Rotation.Y <(-2.0f*M_PI))
        Rotation.Y += 2.0f*M_PI;
    Rotation.Z = fmaxf(-M_PI_4,Rotation.Z);
    Rotation.Z = fminf(M_PI_4,Rotation.Z);
    //Beschleunigung auf Geschwindigkeit anwenden
    Velocity += Acceleration * (1.0f/Mass) * dtime;
    //Geschwindigkeit auf Position verrehcnen
    Position +=  Velocity * dtime ;
    //Transformation updaten
    updateTransform();
}

/*
 * Transformation updaten
 */
void Car::updateTransform(){
   PhysicObject::updateTransform();
}

/*
 * Physik Werte updaten
 */
void Car::update(float dtime, float& omega){
    Vector  fTraction,
            fDrag,
            afDrag,
            fRollResi,
            fLong,
            fFriction;
    //Richtung auf Ausrichtung des Autos setzen
    Direction = transform().forward();
    //Reibungskraft
    fRollResi = Velocity * -ROLL_RESISTANCE;
    //Schubkraft
    fTraction =  Direction*ForwardBackward*10000.0f + (Velocity*Brake*-BRAKE) ;
    //alle Kraefte
    fLong = (fTraction+fRollResi);
    //Kraefte anwenden
    addForce(fLong);
    //bei keiner Kollision Geschwindigkeit in Richtung des Autos drehen
    if(!Collide)
        Velocity = Direction * ((Velocity.dot(Direction))/(Direction.lengthSquared()));
    
    //Lenkungswinkel setzen
    float SteeringAngle = (LeftRight * M_PI_2  * Reverse);
    //omega = Lenkung
    omega = SteeringAngle;
    //Rotation als Impuls ausueben
    Rotation.Y += SteeringAngle*dtime;
    //Lenkung als Kraft auf Z Rotation ausueben um Auto in Kurven kippe zu lassen
    addAngularForce(Vector(0,0,-SteeringAngle*2000.0f));
    
    //Z-Rotation per Zeit wieder auf Null schrumpfen lassen
    float azDrag = -AngularVelocity.Z * (0.02f+dtime);
    AngularVelocity.Z  =  AngularVelocity.Z +  azDrag;
    float zDrag = -Rotation.Z * (0.02f+dtime);
    Rotation.Z = Rotation.Z + zDrag;
    //Physik simulieren
    simulate(dtime);
}
